# Import necessary libraries
import pandas as pd
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler, StandardScaler

# Load the Titanic Dataset
titanic_df = sns.load_dataset("titanic")

# Normalize 'age'
titanic_df["age"] = (titanic_df["age"] - titanic_df["age"].min()) / (
    titanic_df["age"].max() - titanic_df["age"].min()
)

# Display the normalized ages
print(titanic_df["age"])

# Standardize 'fare'
titanic_df["fare"] = (titanic_df["fare"] - titanic_df["fare"].mean()) / titanic_df[
    "fare"
].std()

# Display the standardized fares
print(titanic_df["fare"])


# Select 'age' column and drop NaN values
age = titanic_df[["age"]].dropna()

# Create a MinMaxScaler object
scaler = MinMaxScaler()

# Use the scaler
titanic_df["norm_age"] = pd.DataFrame(
    scaler.fit_transform(age), columns=age.columns, index=age.index
)

# Display normalized age values
print(titanic_df["norm_age"])


# Select 'fare' column and drop NaN values

fare = titanic_df[["fare"]].dropna()


# Create a StandardScaler object

scaler = StandardScaler()


# Use the scaler

titanic_df["stand_fare"] = pd.DataFrame(
    scaler.fit_transform(fare), columns=fare.columns, index=fare.index
)


# Display standardized fare values

print(titanic_df["stand_fare"])
