# Import necessary libraries
import seaborn as sns
from sklearn.preprocessing import StandardScaler

# Load the Titanic Dataset
titanic_df = sns.load_dataset("titanic")

fare = titanic_df[["fare"]].dropna()

# Create a StandardScaler object
scaler = StandardScaler()

# Incorrectly using fit_transform on the 'fare' column with NaN values included
titanic_df["stand_fare"] = scaler.fit(fare)
titanic_df["stand_fare"] = scaler.transform(titanic_df[["fare"]])

# Display standardized fare values
print(titanic_df[["fare", "stand_fare"]])
