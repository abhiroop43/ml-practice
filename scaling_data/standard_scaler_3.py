# Import necessary libraries
import seaborn as sns
from sklearn.preprocessing import StandardScaler

# TODO: Load the Titanic Dataset
titanic_df = sns.load_dataset("titanic")

# TODO: Create a StandardScaler object
fare_scaler = StandardScaler()

# TODO: Fit the scaler on the 'fare' data while handling NaN values properly
fare_scaler.fit(titanic_df[["fare"]].dropna())

# TODO: Transform the 'fare' column creating a new column 'stand_fare' in the original dataframe without NaN values
non_na_fare_index = titanic_df["fare"].dropna().index
titanic_df.loc[non_na_fare_index, "norm_fare"] = fare_scaler.transform(
    titanic_df.loc[non_na_fare_index, ["fare"]]
)

# TODO: Display standardized 'fare' values (the new 'stand_fare' column)
print(titanic_df[["fare", "norm_fare"]])
